﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Chicken_Invaders_NET
{
    class Game
    {
        private Font font;
        //private Texture background;
        private Sprite sprite;
        public GameState state;
        private RenderWindow window;

        public Game()
        {
            state = GameState.END;
            font = new Font("Fonts/Mecha.ttf");
            state = GameState.MENU;
            window = new RenderWindow(new VideoMode(DrawableObject.WindowWidth, DrawableObject.WindowHeight),
                "Chicken Invaders NET");
            sprite = new Sprite();
        }

        public void RunGame()
        {
            while (state != GameState.END)
            {
                switch (state)
                {
                    case GameState.MENU:
                        Menu();
                        break;
                    case GameState.GAME:
                        Single();
                        state = GameState.BEST_SCORES;
                        break;
                    case GameState.BEST_SCORES:
                        Ranking();
                        state = GameState.MENU;
                        break;
                }
            }

        }


        private void Menu()
        {
            window.Clear();
            Texture background;

            background = new Texture("Images/background.png");
            sprite.Texture = (background);

            Text title = new Text("Chicken Invader", font, 80);
            title.Style = Text.Styles.Bold;

            title.Position = new Vector2f(DrawableObject.WindowWidth / 2f - title.GetGlobalBounds().Width / 2, 20);
            const int options_number = 3;

            Text[] tekst = new Text[options_number];

            string[] str = {"Play", "Best Scores", "Exit"};
            for (int i = 0; i < options_number; i++)
            {
                tekst[i] = new Text();
                tekst[i].Font = (font);
                tekst[i].CharacterSize = (70 - 5 * options_number);

                tekst[i].DisplayedString = (str[i]);
                tekst[i].Position = new Vector2f(DrawableObject.WindowWidth / 2f - tekst[i].GetGlobalBounds().Width / 2,
                    260 - 10 * options_number + i * (120 - 10 * options_number));
            }

            while (state == GameState.MENU)
            {
                Vector2i mouse = Mouse.GetPosition(window);
                window.KeyPressed += (sender, args) =>
                {
                  if (args.Code == Keyboard.Key.Escape)
                    {
                        state = GameState.END;
                    }
                };
                window.Closed += (sender, args) => { state = GameState.END; };
                window.MouseButtonPressed += (sender, args) =>
                {
                    if (tekst[0].GetGlobalBounds().Contains(mouse.X, mouse.Y) && args.Button == Mouse.Button.Left)
                    {
                        state = GameState.GAME;
                    }

                    if (tekst[1].GetGlobalBounds().Contains(mouse.X, mouse.Y) && args.Button == Mouse.Button.Left)
                    {
                        state = GameState.BEST_SCORES;
                    }

                    if (tekst[options_number - 1].GetGlobalBounds().Contains(mouse.X, mouse.Y) &&
                        args.Button == Mouse.Button.Left)
                    {
                        state = GameState.END;
                    }
                };

                window.WaitAndDispatchEvents();

                for (int i = 0; i < options_number; i++)
                    if (tekst[i].GetGlobalBounds().Contains(mouse.X, mouse.Y))
                        tekst[i].FillColor = Color.Cyan;
                    else
                        tekst[i].FillColor = (Color.White);

                window.Clear();
                window.Draw(sprite);
                window.Draw(title);
                for (int i = 0; i < options_number; i++)
                    window.Draw(tekst[i]);

                window.Display();
            }

        }

        private void Single()
        {
            new Engine(window);
        }

        private void Ranking()
        {
            window.Clear();
           // Texture background;
            //background = new Texture("Images/background.png");
            //sprite.Texture = (background);

            Text title = new Text("Best Scores", font, 80);
            title.Style = Text.Styles.Bold;
            title.Position = new Vector2f(DrawableObject.WindowWidth / 2 - title.GetGlobalBounds().Width / 2, 20);

            List<string> file = new List<string>();
            if(File.Exists("bestScores.txt"))
            {
                file = File.ReadAllLines("bestScores.txt").ToList();
            }

            const int max_person = 5;
            string line;
            List<Text> person = new List<Text>();
            List<Text> points = new List<Text>();

            int count = 0;
            bool what = true;
            while (count < 2*max_person && count < file.Count)
            {
                line = file[count];
                if (what)
                {
                   
                    Text t = new Text(line, font, 60);
                    t.Style = Text.Styles.Bold;
                    person.Add(t);
                    what = false;
                    count++;
                }
                else
                {
                    Text t = new Text(line, font, 60);
                    t.Style = Text.Styles.Bold;

                    points.Add(t);
                    
                    what = true;
                    count++;
                }
            }
 
            for (int i = 0; i < person.Count && i < max_person && i < points.Count ; i++)
            {

                person[i].Position =
                    new Vector2f(DrawableObject.WindowWidth / 2f - person[i].GetGlobalBounds().Width - 20,
                        260 - 10 * max_person + i * (120 - 10 * max_person));
                points[i].Position = new Vector2f(DrawableObject.WindowWidth / 2 + 20,
                    260 - 10 * max_person + i * (120 - 10 * max_person));

            }


            while (state == GameState.BEST_SCORES)
            {
                
                window.Clear();
                    window.Draw(sprite);
                    window.Draw(title);
                    for (int i = 0; i < person.Count &&  i < points.Count && i < max_person; i++)
                    {

                    window.Draw(person[i]);
                        window.Draw(points[i]);
                    }

                    window.Display();
                window.WaitAndDispatchEvents();
                window.KeyPressed += (sender, args) =>
                {
                    if (args.Code == Keyboard.Key.Escape)
                        state = GameState.MENU;
                };


            }

        }


        public enum GameState
        {
            MENU,
            GAME,
            BEST_SCORES,
            END
        }
    }
}
