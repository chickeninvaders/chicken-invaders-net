﻿using SFML.Graphics;
using SFML.System;

namespace Chicken_Invaders_NET
{
    class HealthPoints : DrawableObject
    {
       // private Font font;

        public HealthPoints(float t_x, string itemName)
        {
            if (itemName == "HP")
            {
                sprite.Position = new Vector2f(t_x + 10, 10);
                texture = new Texture("Images/HP.png");
                sprite.Texture = texture;
            }
        }
    }
}
