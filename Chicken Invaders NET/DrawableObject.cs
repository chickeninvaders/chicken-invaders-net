﻿using SFML.Graphics;

namespace Chicken_Invaders_NET
{
    class DrawableObject : Transformable, Drawable
    {
        public const int WindowWidth = 800;
        public const int WindowHeight = 600;

        protected Texture texture;
        protected Sprite sprite;

        protected DrawableObject()
        {
            sprite = new Sprite();
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(sprite, states);
        }
    }
}
