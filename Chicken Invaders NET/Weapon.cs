﻿using SFML.Graphics;
using SFML.System;
using System;

namespace Chicken_Invaders_NET
{
    class Weapon : DrawableObject
    {
        private float weaponVelocity = 7;
        private Vector2f velocity = new Vector2f(0, -7);
        private bool destroyed = false;
        public bool isNew = true;
        public string defaultClass = "";
        


        public Weapon(string kindOfClass)
        {
            if (kindOfClass == "Chicken")
            {
                texture = new Texture("Images/Egg.png");
            }
            else if (kindOfClass == "Ship")
            {
                texture = new Texture("Images/Ammo.png");
            }
            else if (kindOfClass == "Bonus")
            {
                texture = new Texture("Images/Drumstick.png");
            }
            else if (kindOfClass == "BossChicken")
            {
                texture = new Texture("Images/BossEgg.png");
            }
            else if (kindOfClass == "Shield")
            {
                texture = new Texture("Images/BonusShield.png");
            }
            else if (kindOfClass == "Upgrade")
            {
                texture = new Texture("Images/UpgradeAmmo.png");
            }
            sprite.Texture  = texture;
            defaultClass = kindOfClass;
        }

        public void Update(Vector2f position)
        {
            if (this.isNew)
            {
                sprite.Position = position;
                isNew = false;
            }
            else if (this.Top() > WindowHeight || this.Bottom() < 0)
            {
                this.Destroy();
            }

            if (this.defaultClass == "Chicken" || this.defaultClass == "Shield" || this.defaultClass == "Upgrade")
            {
                velocity = new Vector2f(0, weaponVelocity);
            }
            else if (this.defaultClass == "BossChicken")
            {
                velocity = new Vector2f(0, weaponVelocity / 2f);
            }
            else if (this.defaultClass == "Bonus")
            {
                weaponVelocity = 3.0f;
                velocity = new Vector2f(0, weaponVelocity);
            }
            sprite.Position = sprite.Position + velocity;

        }

        public void Update(Vector2f position, int direction)
        {
            if (this.isNew)
            {
                Console.Write(position + "\n");
                sprite.Position = position;
                if (direction == 0)
                    sprite.Rotation = -20;
                if (direction == 2)
                    sprite.Rotation = 20;
                isNew = false;
            }
            else if (this.Top() > WindowHeight || this.Bottom() < 0)
            {
                this.Destroy();
            }

            switch (direction)
            {
                case 0:
                    velocity = new Vector2f(-0.2f*weaponVelocity, -weaponVelocity);
                    break;
                case 1:
                    velocity = new Vector2f(0, -weaponVelocity);
                    break;
                case 2:
                    velocity = new Vector2f(0.2f * weaponVelocity, -weaponVelocity);
                    break;
            }

            sprite.Position = sprite.Position + velocity;
        }

        public Vector2f GetPosition()
        {
            return sprite.Position;
        }

        public float Left()
        {
            return sprite.Position.X;
        }
        public float Right()
        {
            return sprite.Position.X + texture.Size.X;
        }
        public float Top()
        {
            return sprite.Position.Y;
        }
        public float Bottom()
        {
            return sprite.Position.Y + texture.Size.Y;
        }

        public bool IsDestroyed()
        {
            return destroyed;
        }

        public void Destroy()
        {
            destroyed = true;
        }
    }
}
