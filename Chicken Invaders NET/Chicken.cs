﻿using SFML.Graphics;
using SFML.System;

namespace Chicken_Invaders_NET
{
    class Chicken : DrawableObject
    {
        private Texture texture;
        private Texture bossTexture;
        private float chickenVelocity = 1;
        private Vector2f velocity;
        private bool destroyed = false;

        public int lifePoints;
        public bool verticalDirection;
        public bool horizontalDirection;

        public Chicken(float t_X, float t_Y, int level)
        {
            sprite.Position = new Vector2f(t_X, t_Y);
            if (level == 1)
            {
                texture = new Texture("Images/LittleChicken.png");
            }
            else
            {
                if (level < 6)
                {
                    texture = new Texture("Images/LittleChicken" + level +".png");
                }
                else
                {
                    texture = new Texture("Images/Boss.png");
                }
            }
            sprite.Texture = texture;
        }

        public void Update()
        {
            if (!verticalDirection)
            {
                velocity.X = -chickenVelocity;
            }
            else if (verticalDirection)
            {
                velocity.X = chickenVelocity;
            }

            if (!horizontalDirection)
            {
                velocity.Y = -chickenVelocity;
            }
            else if (horizontalDirection)
            {
                velocity.Y = chickenVelocity;
            }



            if (bossTexture != null && !(texture).Size.Equals( (bossTexture).Size))
            {
                if (lifePoints == 1)
                    texture = new Texture("Images/LittleChicken.png");
                else if (lifePoints == 2)
                    texture = new Texture("Images/LittleChicken2.png");
                else if (lifePoints == 3)
                    texture = new Texture("Images/LittleChicken3.png");
                else if (lifePoints == 4)
                    texture = new Texture("Images/LittleChicken4.png");
                else if (lifePoints == 5)
                    texture = new Texture("Images/LittleChicken5.png");
            }

            sprite.Texture = texture;
            sprite.Position = sprite.Position + velocity;
        }

        public Vector2f GetPosition()
        {
            return sprite.Position;
        }

        public float Left()
        {
            return sprite.Position.X;
        }
        public float Right()
        {
            return sprite.Position.X + texture.Size.X;
        }
        public float Top()
        {
            return sprite.Position.Y;
        }
        public float Bottom()
        {
            return sprite.Position.Y + texture.Size.Y;
        }

        public bool IsDestroyed()
        {
            return destroyed;
        }

        public void Destroy()
        {
            if (--lifePoints == 0)
                destroyed = true;
        }
    }
}
