﻿namespace Chicken_Invaders_NET
{
    class Counter
    {
        public int points;

        public Counter(int numb)
        {
            SetPoints(numb);
        }

        public int ViewPoints()
        {
            return points;
        }

        public void SetPoints(int numb)
        {
            points = numb;
        }

        public static Counter operator +(Counter counter, int diff)
        {
            counter.points += diff;
            return counter;
        }
    }
}
