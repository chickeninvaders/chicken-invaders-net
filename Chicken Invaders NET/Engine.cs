﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Text;
using System.Threading;


namespace Chicken_Invaders_NET
{
    class Engine : Transformable
    {
        public const int WindowWidth = 800;
        public const int WindowHeight = 600;

       // public StreamReader inFile;
        //public StreamWriter outFile;

        private Font font = new Font("Fonts/Mecha.ttf");
        private string[] str_inscription = new string[3];
        private Text[] inscription = new Text[3];



        private Ship ship;

        private Texture background = new Texture("Images/background.png");
        private Sprite sprite;

        private Clock clockForEggs = new Clock();
        private Clock clockForBossEggs = new Clock();
        private Clock clockForAmmo = new Clock();
        private Clock clockForScore = new Clock();
        private Clock clockForShield = new Clock();
        private Clock levelGameTimer = new Clock();

        private float frequencyOfEggs = 0.5f ;
        private float frequencyOfBossEggs = 3.0f;
        private float frequencyOfAmmo = 0.1f;
        private float frequencyOfScore = 0.5f;
        private float periodOfShield = 5.0f;
        //private float GameTimer = 0.0f;


        private List<Weapon> [] ammo = {new List<Weapon>(),new List<Weapon>(),new List<Weapon>()   };
        private List<Weapon> eggs = new List<Weapon>();
        private List<Weapon> bonuses = new List<Weapon>();
        private List<Weapon> upgrades = new List<Weapon>();
        private List<Weapon> shields = new List<Weapon>();
        private List<Chicken> chickens = new List<Chicken>();
        private List<HealthPoints> hearts = new List<HealthPoints>();



        public Engine(RenderWindow window)
        {

            ship = new Ship();
            sprite = new Sprite(background);


            str_inscription[0] = "Points: ";
            str_inscription[1] = "";
            str_inscription[2] = "Shield: ";

            for (int i = 0; i < 3; i++)
            {
                inscription[i] = new Text();
                inscription[i].FillColor = new Color(255, 255, 255, 255);
            }
            inscription[0] = new Text(str_inscription[0] + ToString(ship.score.ViewPoints()), font, 30);
            inscription[0].Position = new Vector2f(WindowWidth - inscription[0].GetGlobalBounds().Width - 10, 10);

            inscription[1] = new Text("0.0sec", font, 30);
            inscription[1].Position = new Vector2f(WindowWidth - inscription[1].GetGlobalBounds().Width / 2, 10);

            RunEngine(window);
        }
        
        public void RunEngine(RenderWindow window)
        {
            bool nextLevel = false;
            window.SetFramerateLimit(60);

            bool endGame = false;

            int heartWidth = 30;

            for (int i = 0; i < ship.lifePoints; i++)
                hearts.Add(new HealthPoints(heartWidth * i, "HP"));


            for (int level = 1; level <= 6 && !endGame && hearts.Count() != 0; level++)
            {
                TransitionBetweenLevels(level, window);

                nextLevel = false;
                bool safe = ship.IfSafe(false);

                ArrangementOfChicken(level);

                while (!nextLevel && !endGame && ship.HP.ViewPoints() > 0)
                {
                    window.Clear();

                    window.Closed += (sender, args) => { window.Close(); };

                    //window.WaitAndDispatchEvents();

                    window.KeyReleased += (sender, args) =>
                    {
                        if (args.Code == Keyboard.Key.Escape)
                        {
                            endGame = true;
                        }
                    };

                    CheckingTime(level, safe);
                    Interactions(level, safe);
                    Update(safe);
                    RemoveIf();

			        if (chickens.Count == 0)
				        nextLevel = true;

			        Draw(window);

                    window.Display();
			
		        }   

                RemoveTheResidue();
            }
            EndTheGame(window);
            AddToScores(window);
        }

        private bool IsIntersecting(Ship A, Weapon B)
        {
            return A.Right() >= B.Left() && A.Left() <= B.Right()
                && A.Bottom() >= B.Top() && A.Top() <= B.Bottom();
        }
        private bool IsIntersecting(Chicken A, Weapon B)
        {
            return A.Right() >= B.Left() && A.Left() <= B.Right()
                && A.Bottom() >= B.Top() && A.Top() <= B.Bottom();
        }
        private bool IsIntersecting(Weapon A, Weapon B)
        {
            return A.Right() >= B.Left() && A.Left() <= B.Right()
                && A.Bottom() >= B.Top() && A.Top() <= B.Bottom();
        }
        private void Interactions(int level, bool safe)
        {
            for (int direction = 0; direction < 3; direction++)
            {
                for (int i = 0; i < ammo[direction].Count; i++)
                    for (int j=0; j < chickens.Count; j++)
                    {
                        if (IsIntersecting(chickens[j], ammo[direction][i]))
                        {
                            chickens[j].Destroy();
                            ammo[direction][i].Destroy();
                            if (chickens[j].lifePoints == 0)
                            {
                                if (level == 6)
                                    ship.score += 250;
						else
							ship.score += (5 * level);
                                inscription[0] = new Text(str_inscription[0] + ToString(ship.score.ViewPoints()),font);
                                inscription[0].Position = new Vector2f(WindowWidth - inscription[0].GetGlobalBounds().Width - 10, 10);
                                break;
                            }
                        }
                    }
            }

            for (int direction = 0; direction < 3; direction++)
                for (int i = 0; i < ammo[direction].Count; i++)
                    for (int j = 0; j < eggs.Count; j++)
                    {
                        if (IsIntersecting(eggs[j], ammo[direction][i]))
                        {
                            ammo[direction][i].Destroy();
                        }
                    }

            for (int j = 0; j < eggs.Count; j++)
                if (IsIntersecting(ship, eggs[j]))
                {
                    eggs[j].Destroy();
                    if (ship.IfSafe(safe))
                    {
                        safe =  false ;
                        break;
                    }
                    if (ship.ammoLevel >= 2)
                    {
                        ship.ammoLevel--;
                    }
                    ship.Destroy();
                    hearts.RemoveAt(hearts.Count - 1);
                }

            for (int i = 0; i < bonuses.Count; i++)
                if (IsIntersecting(ship, bonuses[i]))
                {
                    bonuses[i].Destroy();
                    ship.score += (5);
                    inscription[0] = new Text(str_inscription[0] + ToString(ship.score.ViewPoints()), font);
                    inscription[0].Position = new Vector2f(WindowWidth - inscription[0].GetGlobalBounds().Width - 10, 10);
                }

            for (int i = 0; i < shields.Count; i++)
                if (IsIntersecting(ship, shields[i]))
                {
                    shields[i].Destroy();
                    safe = ship.IfSafe(true);
                    clockForShield.Restart();
                }

            for (int i = 0; i < upgrades.Count; i++)
                if (IsIntersecting(ship, upgrades[i]))
                {
                    upgrades[i].Destroy();
                    if (ship.ammoLevel <= 3)
                    {
                        ship.ammoLevel++;
                    }
                }
        }
        private int ToInt(string line)
        {
            return Int32.Parse(line);
        }
        public string ToString(float number)
        {
            return number.ToString();
        }
        public string ToString(int number)
        {
            return number.ToString();
        } 
        private float GetSpecialEffectTime(bool safe)
        {
            if (!ship.IfSafe(safe))
                return 0;

            float t = periodOfShield - clockForShield.ElapsedTime.AsSeconds();
            return t;
        }
        private void RemoveTheResidue()
        {
            for (int direction = 0; direction < 3; direction++)
            {
                for (int i = 0; i < ammo[direction].Count; i++)
                    ammo[direction][i].Destroy();
            }

            for (int i=0; i<eggs.Count; i++)
                eggs[i].Destroy();

            for (int i = 0; i < bonuses.Count; i++)
                bonuses[i].Destroy();

            for (int i = 0; i < shields.Count; i++)
                shields[i].Destroy();

            for (int i = 0; i < upgrades.Count; i++)
                upgrades[i].Destroy();   

            RemoveIf();
        }
        private void RemoveIf()
        {
            for(int i=0; i<chickens.Count; i++)
            {
                if(chickens[i].IsDestroyed())
                {
                    chickens.RemoveAt(i);
                    i--;
                }
            }

            for (int i = 0; i < eggs.Count; i++)
            {
                if (eggs[i].IsDestroyed())
                {
                    eggs.RemoveAt(i);
                    i--;
                }
            }

            for (int direction = 0; direction < 3; direction++)
            {
                for (int i = 0; i < ammo[direction].Count; i++)
                {
                    if (ammo[direction][i].IsDestroyed())
                    {
                        ammo[direction].RemoveAt(i);
                        i--;
                    }
                }
            }

            for (int i = 0; i < bonuses.Count; i++)
            {
                if (bonuses[i].IsDestroyed())
                {
                    bonuses.RemoveAt(i);
                    i--;
                }
            }

            for (int i = 0; i < shields.Count; i++)
            {
                if (shields[i].IsDestroyed())
                {
                    shields.RemoveAt(i);
                    i--;
                }
            }

            for (int i = 0; i < upgrades.Count; i++)
            {
                if (upgrades[i].IsDestroyed())
                {
                    upgrades.RemoveAt(i);
                    i--;
                }
            }
        }
        private void ChickenDirection(List<Chicken> chicken)
        {
            for(int i=0; i<chicken.Count; i++)
            {
                if (chicken[i].Left() <= 0)
                    for (int j = 0; j < chickens.Count; j++)
                    {
                        chicken[j].verticalDirection = true;
                    }

                else if (chicken[i].Right() >= WindowWidth)
                    for (int j = 0; j < chickens.Count; j++)
                    {
                        chicken[j].verticalDirection = false;
                    }

                if (chicken[i].Top() <= 30)
                    for (int j = 0; j < chickens.Count; j++)
                    {
                        chicken[j].horizontalDirection = true;
                    }

                else if (chicken[i].Bottom() >= (WindowHeight / 2.0f) + 30)
                    for (int j = 0; j < chickens.Count; j++)
                    {
                        chicken[j].horizontalDirection = false;
                    }
            }

        }
        private void ArrangementOfChicken(int level)
        {
            if (level < 6)
            {
                int chickenX = 14, chickenY =  3, chickenWidth = 44, chickenHeight = 56;

                for (int i = 0; i < chickenY; i++)
                    for (int j = 0; j < chickenX; j++)
                        chickens.Add(new Chicken((j + 1) * (chickenWidth + 8), (i + 1) * (chickenHeight + 37), level));

                for (int i = 0; i < chickens.Count; i++)
                    chickens[i].lifePoints = level;
            }
            else
            {
                chickens.Add(new Chicken(WindowWidth / 8, WindowHeight / 6, level));
                chickens[0].lifePoints = 200;
            }
        }
        private void TransitionBetweenLevels(int level, RenderWindow window)
        {
            int mod = 5, limit = 255;
            RectangleShape rect = new RectangleShape();
            rect.Size = new Vector2f(WindowWidth, WindowHeight);
            rect.FillColor = new Color(0, 0, 0, 0);

            for (var alpha = 0; alpha < limit; alpha += mod)
            {
                window.Clear();
                rect.FillColor = new Color(0, 0, 0, Convert.ToByte(alpha));
                window.Draw(sprite);
                window.Draw(rect);
                //Thread.Sleep(30);
                window.Display();
            }
            if (level < 6)
            {
                Text text = new Text("Level nr:", font, 80);
                text.Style = Text.Styles.Bold;
                text.Position = new Vector2f(WindowWidth / 2 - text.GetGlobalBounds().Width / 2, 150);
                window.Draw(text);
                Text number = new Text(ToString(level), font, 80);
                text.Style = Text.Styles.Bold;
                number.Position = new Vector2f(WindowWidth / 2 - number.GetGlobalBounds().Width / 2, 250);
                window.Draw(number);
            }
            else
            {
                Text text = new Text("Get ready!", font, 80);
                text.Style = Text.Styles.Bold;
                text.Position = new Vector2f(WindowWidth / 2 - text.GetGlobalBounds().Width / 2, 150);
                window.Draw(text);
                Text number = new Text("BOSS", font, 120);
                text.Style = Text.Styles.Bold;
                number.Position = new Vector2f(WindowWidth / 2 - number.GetGlobalBounds().Width / 2, 300);
                window.Draw(number);
            }
            window.Display();
            //Thread.Sleep(2000);

            for (int alpha = 255; alpha > 0; alpha -= mod)
            {
                window.Clear();
                rect.FillColor = new Color(0, 0, 0, Convert.ToByte(alpha));
                window.Draw(sprite);
                window.Draw(rect);
                //Thread.Sleep(30);
                window.Display();
            }
            //Thread.Sleep(1000);
        }
        private void Draw(RenderWindow window)
        {
            window.Draw(sprite);

            for (int i = 0; i < 3; i++)
            {
                window.Draw(inscription[i]);
            }

            for (int direction = 0; direction < 3; direction++)
            {
                for (int i = 0; i < ammo[direction].Count; i++)
                    ammo[direction][i].Destroy();
            }

            for (int i = 0; i < eggs.Count; i++)
                window.Draw(eggs[i]);

            for (int i = 0; i < bonuses.Count; i++)
                window.Draw(bonuses[i]);

            for (int i = 0; i < shields.Count; i++)
                window.Draw(shields[i]);

            for (int i = 0; i < upgrades.Count; i++)
                window.Draw(upgrades[i]);

            window.Draw(ship);

            for (int i = 0; i < chickens.Count; i++)
                window.Draw(chickens[i]);

            for (int i = 0; i < hearts.Count; i++)
                window.Draw(hearts[i]);
        }
        private void Update(bool safe)
        {
            if (GetSpecialEffectTime(safe) == 0)
            {

                Color color = inscription[2].FillColor;
                if ((int)color.A > 0)
                {
                    color.A -= 5;
                    inscription[2].FillColor = new Color(255, 255, 255, color.A);
                }
            }
            else
            {
                Color color = inscription[2].FillColor;
                if ((int)color.A < 255)
                {
                    color.A += 5;
                    inscription[2].FillColor = new Color(255, 255, 255, color.A);
                }
            }

            inscription[2] = new Text(str_inscription[2] + ToString(GetSpecialEffectTime(safe)) + "s", font);
            inscription[2].Position = new Vector2f(WindowWidth - 10 - inscription[2].GetGlobalBounds().Width, WindowHeight - 30);
            Random rand = new Random();
            int randChicken = rand.Next(chickens.Count);

            ChickenDirection(chickens);

            for (int direction = 0; direction < 3; direction++)
                for (int i = 0; i < ammo[direction].Count; i++)
                    ammo[direction][i].Update(ship.GetPosition(), direction);
            for (int i = 0; i < eggs.Count; i++)
                eggs[i].Update(chickens[randChicken].GetPosition());

            ship.Update(safe);

            for (int i = 0; i < chickens.Count; i++)
                chickens[i].Update();

            for (int i = 0; i < chickens.Count; i++)
            {
                if (chickens[i].IsDestroyed())
                {
                    int randomInt = rand.Next(21);
                    if (randomInt == 0)
                    {
                        shields.Add(new Weapon("Shield"));
                        shields[shields.Count - 1].Update(chickens[i].GetPosition());
                    }
                    else if (randomInt == 10)
                    {
                        upgrades.Add(new Weapon ("Upgrade"));
                        upgrades[upgrades.Count - 1].Update(chickens[i].GetPosition());
                    }

                    bonuses.Add(new Weapon ("Bonus"));
                    bonuses[bonuses.Count - 1].Update(chickens[i].GetPosition());
                }
            }

            for (int i = 0; i < bonuses.Count; i++)
                bonuses[i].Update(chickens[chickens.Count - 1].GetPosition());

            for (int i = 0; i < shields.Count; i++)
                shields[i].Update(chickens[chickens.Count - 1].GetPosition());

            for (int i = 0; i < upgrades.Count; i++)
                upgrades[i].Update(chickens[chickens.Count - 1].GetPosition());

            float time = levelGameTimer.ElapsedTime.AsSeconds();
            inscription[1] = new Text(ToString(time) + "s",font);
            inscription[1].Position = new Vector2f(WindowWidth / 2 - inscription[1].GetGlobalBounds().Width / 2, 10);
        }
        private void CheckingTime(int level, bool safe)
        {
            if (clockForEggs.ElapsedTime.AsSeconds() >= frequencyOfEggs)
            {
                eggs.Add(new Weapon("Chicken"));
                clockForEggs.Restart();

            }
            if (level == 6 && clockForBossEggs.ElapsedTime.AsSeconds() >= frequencyOfBossEggs)
            {
                eggs.Add(new Weapon("BossChicken"));
                clockForBossEggs.Restart();

            }

            if (clockForAmmo.ElapsedTime.AsSeconds() >= frequencyOfAmmo)
            {
                
                Weapon e = new Weapon("Ship");
                if (ship.ammoLevel == 1)
                {
                    ammo[1].Add(e);
                }
                
                else if (ship.ammoLevel == 2)
                {
                    ammo[0].Add(e);
                    ammo[2].Add(e);
                }
                else
                {
                    ammo[0].Add(e);
                    ammo[1].Add(e);
                    ammo[2].Add(e);
                }
                clockForAmmo.Restart();
            }

            if (clockForScore.ElapsedTime.AsSeconds() >= frequencyOfScore)
            {
                if (ship.score.ViewPoints() > 0)
                    ship.score = ship.score + (-1);

                inscription[0].DisplayedString = str_inscription[0] + ToString(ship.score.ViewPoints());
                inscription[0].Position = new Vector2f(WindowWidth - inscription[0].GetGlobalBounds().Width - 10, 10);
                clockForScore.Restart();
            }
            if (clockForShield.ElapsedTime.AsSeconds() >= periodOfShield && safe)
            {
                safe = false;
            }
        }
        private void AddToScores(RenderWindow window)
        {
            window.Clear();
            sprite.Texture = background;
            Text title = new Text("Type in your name", font, 80);
            title.Style = Text.Styles.Bold;
            title.Position = new Vector2f(WindowWidth / 2 - title.GetGlobalBounds().Width / 2, 80);

            Vector2f k = new Vector2f(500.0f, 130.0f);
            RectangleShape rectangle = new RectangleShape(k);
            rectangle.Position = new Vector2f(WindowWidth / 2 - rectangle.Size.X / 2, WindowHeight / 2);
            rectangle.FillColor = new Color(210, 190, 190, 150);


            const int max_person = 5;
            string line;
            List<string> person = new List<string>();
            List<int> points = new List<int>();

            bool what = true;

            if (File.Exists("bestScores.txt"))
            {
                StreamReader inFile = new StreamReader("bestScores.txt");
               // if (!inFile.EndOfStream) Console.Write("Loading failed: bestScores.txt");

                while (!inFile.EndOfStream && points.Count < max_person)
                {
                   
                    line = inFile.ReadLine();
                    if (what)
                    {
                        person.Add(line);
                        if (person.Last() == "")
                        {
                            person.RemoveAt(person.Count - 1);
                            break;
                        }

                     

                        what = false;
                    }
                    else
                    {
                        points.Add(ToInt(line));
                        what = true;

                    }
                }
                inFile.Close();
            }
           

            int maxTextLength = 8;

            string str = "";
            Text text = new Text();
            text.Font = font;
            text.CharacterSize = 50;
            text.Position = new Vector2f( 300 , 330 );
            
            while (window.IsOpen)
            {
               
                Event newEvent = new Event();
                window.Closed += (sender, args) => { window.Close(); };
                window.KeyReleased += (sender, args) =>
                {
                    window.WaitAndDispatchEvents();
                   
                    if (args.Code == Keyboard.Key.BackSpace && str.Length > 0)
                    {
                        str.Remove(str.Length - 1, 1);
                        text.DisplayedString = str;
                       
                    }

                    else if(str.Length < maxTextLength && args.Code.GetHashCode() <= 25 && args.Code.GetHashCode() >= 0)
                    {
                        str += args.Code.ToString();
                        text.DisplayedString = str;
                        
                    }
                    
                    
                };

                window.Clear(new Color(47, 47, 47));
                window.Draw(sprite);
                window.Draw(title);
                window.Draw(rectangle);
                window.Draw(text);
                window.Display();
                
                if (Keyboard.IsKeyPressed(Keyboard.Key.Return) && str.Length > 0)
                {

                    for (int i = 0; i < points.Count; i++)
                    {
                        if (ship.score.ViewPoints() > points[i])
                        {
                            points.Insert(points.IndexOf(points.First()) + i, ship.score.ViewPoints());
                            person.Insert(person.IndexOf(person.First()) + i, str);
                            break;


                        }

                        if (i + 1 == points.Count)
                        {
                            points.Add(ship.score.ViewPoints());
                            person.Add(str);
                            break;
                        }
                    }

                    if (points.Count == 0)
                    {
                        points.Add(ship.score.ViewPoints());
                        person.Add(str);
                    }

                    break;
                }
                window.WaitAndDispatchEvents();
            }
            

            StreamWriter outFile = new StreamWriter("bestScores.txt");
            
            for (int i = 0; i < points.Count; i++)
            {
                outFile.WriteLine(person[i]);
                outFile.WriteLine(ToString(points[i]));
               
            }
            outFile.Close();

        }
        private void EndTheGame(RenderWindow window)
        {
            RectangleShape rect = new RectangleShape();
            rect.Size = new Vector2f(WindowWidth, WindowHeight);
            rect.FillColor = new Color(0, 0, 0, 0);
            Thread.Sleep(2000);
            for (byte alpha = 0; alpha < 255; alpha += 5)
            {
                window.Clear();
                rect.FillColor = new Color(0, 0, 0, alpha);
                window.Draw(sprite);
                window.Draw(rect);
                Thread.Sleep(10);
                window.Display();
            }
            for (byte alpha = 255; alpha > 0; alpha -= 5)
            {
                window.Clear();
                rect.FillColor = new Color(0, 0, 0, alpha);
                window.Draw(sprite);
                window.Draw(rect);
                Thread.Sleep(10);
                window.Display();
            }
            Thread.Sleep(1000);
            Text text = new Text("Your  score is:", font, 80);
            text.Style = Text.Styles.Bold;
            text.Position = new Vector2f(WindowWidth / 2 - text.GetGlobalBounds().Width / 2, 110);
            window.Draw(text);

            Text number = new Text(ToString(ship.score.ViewPoints()), font, 120);
            number.Style = Text.Styles.Bold;
            number.Position = new Vector2f(WindowWidth / 2 - number.GetGlobalBounds().Width / 2, 240);
            window.Draw(number);

            Text text2 = new Text("Congratulations!!!", font, 80);
            text2.Style = Text.Styles.Bold;
            text2.Position = new Vector2f(WindowWidth / 2 - text2.GetGlobalBounds().Width / 2, 410);
            window.Draw(text2);
            window.Display();

            Thread.Sleep(4000);
        }

    }
}
