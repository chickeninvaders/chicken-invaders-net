﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Chicken_Invaders_NET
{
    class Ship : DrawableObject
    {
        public const int SpaceWidth = 72;
        public const int SpaceHeight = 71;

        private Clock anim_clock;
        private float shipVelocity = 6;
        private Vector2f velocity = new Vector2f(6, 6);
        private Texture spaceshipTexture = new Texture("Images/Spaceship.png");
        private Texture shieldTexture = new Texture("Images/Shield.png");


        public Counter HP;
        public Counter score;
        public int lifePoints = 5;
        public int ammoLevel = 1;

        public Ship()
        {
            texture = spaceshipTexture;
            sprite.Texture = texture;
            sprite.Origin = new Vector2f(texture.Size.X/2f, 0);
            sprite.Position = new Vector2f(WindowWidth/2f, WindowHeight - 80);
            
            anim_clock = new Clock();
            anim_clock.Restart();
            HP = new Counter(lifePoints);
            score = new Counter(0);
        }

        public void Update(bool safe)
        {
            IfSafe(safe);
            sprite.Position = sprite.Position + velocity;

            if (Keyboard.IsKeyPressed(Keyboard.Key.Left) && this.Left() > 0)
            {
                velocity.X = -shipVelocity;
            }
            else if (Keyboard.IsKeyPressed(Keyboard.Key.Right) && this.Right() < WindowWidth)
            {
                velocity.X = shipVelocity;
            }
            else
            {
                velocity.X = 0;
            }

            if (Keyboard.IsKeyPressed(Keyboard.Key.Up) && this.Top() > (WindowHeight / 2f) + 30)
            {
                velocity.Y = -shipVelocity;
            }
            else if (Keyboard.IsKeyPressed(Keyboard.Key.Down) && this.Bottom() < WindowHeight)
            {
                velocity.Y = shipVelocity;
            }
            else
            {
                velocity.Y = 0;
            }
        }

        public float Left()
        {
            return sprite.Position.X;
        }
        public float Right()
        {
            return sprite.Position.X + texture.Size.X;
        }
        public float Top()
        {
            return sprite.Position.Y;
        }
        public float Bottom()
        {
            return sprite.Position.Y + texture.Size.Y;
        }
        public void Destroy()
        {
            HP = HP + -1;
            sprite.Position = new Vector2f(WindowWidth/2f, WindowHeight-80);

        }
        public bool IfSafe(bool safe)
        {
            if (safe)
            {
                texture = shieldTexture;
            }
            else
            {
                texture = spaceshipTexture;
                    ;
            }
            sprite.Texture = texture;
            return safe;
        }
        public Vector2f GetPosition()
        {
            return sprite.Position;
        }
    }
}
